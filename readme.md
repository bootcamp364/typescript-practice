- install typescript: `npm i -g typescript`
- checking version of ts: `tsc -v`
- complie ts file: `tsc index.ts` or `tsc`
- compile in watch mode: `tsc -w`

## Topics

- Providing types to variables
- Providing types to functions
- Union Operator
- Custom Types
  - Interfaces
  - Type aliases
- Type Assertion / Type Casting
- Data Types: void (undefined, null), unknowm, any, never
- Ts with DOM: use as operator to specify what type and make it less generic
- Classes
  - private
  - protected => can be used in children classes
  - public
  - readonly
  - implements <interface_name>
  - static => only accessible in class itself not instances
  - inheritance `class CLASS_NAME_1 extends CLASS_NAME_2`
- Generics
  - Interfaces
  - Functions
- Enums
