const hi = 'people';
let hello: string = 'world';

const getFullName = (name: string, surname: string): string => {
  return name + ' ' + surname;
};

// INTERFACE - CUSTOM TYPE
interface UserInterface {
  name: string;
  age?: number; // optional
  getMessage(): string;
}

const user1: { name: string; age: number } = {
  name: 'Monster',
  age: 30,
};

const user2: UserInterface = {
  name: 'Jack',
  age: 24,
  getMessage() {
    return 'Hello ' + this.name;
  },
};

// TYPE ALIAS - CUSTOM TYPE
type ID = string;
interface FlightInterface {
  id: ID;
  flightNumber: number;
}

// UNION
let pageNumber: number | null = null; // union with type
let userDetail: UserInterface | null = null; // union with custom type (interface)
let idDetail: ID | null = null; // union with custom type (type alias)

// TYPE ASSERTION / TYPE CASTING
let pageNo: string = '1';
let numericPageNo: number = pageNo as unknown as number;

// WORKING WITH DOM
const somElement = document.querySelector('.foo') as HTMLInputElement;
console.log(somElement.value);

// CLASSES
interface UserInterface_ {
  getFullName(): string;
}
class UserClass implements UserInterface_ {
  private fname: string;
  private lname: string;
  static readonly maxAge = 50;
  readonly unchangableFname: string; // contant variable
  constructor(fname: string, lname: string) {
    this.fname = fname;
    this.lname = lname;
    this.unchangableFname = fname;
  }
  getFullName(): string {
    return this.fname + ' ' + this.lname;
  }
}
class AdminClass extends UserClass {}

const user = new UserClass('Tanisha', 'Bisht');
console.log(UserClass.maxAge); // static

// GENERICS WITH FUNCTION
const addId = <T extends object>(obj: T) => {
  const id = Math.random().toString(16);
  return { ...obj, id };
};
interface objInterface {
  name: string;
}
const objData: objInterface = { name: 'Tanisha' };
const result = addId<objInterface>(objData);
console.log('result', result);

// GENERICS WITH INTERFACE
interface genericInterface<T> {
  name: string;
  data: T;
}
const genericData: genericInterface<number> = {
  name: 'Tanisha',
  data: 347,
};

// ENUMS: can be used as data type as well
enum Status {
  NotStarted,
  InProgress,
  Done,
}
let notStartedStatus: Status = Status.NotStarted;
console.log(Status.Done); // 2
