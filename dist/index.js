var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var hi = 'people';
var hello = 'world';
var getFullName = function (name, surname) {
    return name + ' ' + surname;
};
var user1 = {
    name: 'Monster',
    age: 30
};
var user2 = {
    name: 'Jack',
    age: 24,
    getMessage: function () {
        return 'Hello ' + this.name;
    }
};
// UNION
var pageNumber = null; // union with type
var userDetail = null; // union with custom type (interface)
var idDetail = null; // union with custom type (type alias)
// TYPE ASSERTION / TYPE CASTING
var pageNo = '1';
var numericPageNo = pageNo;
// WORKING WITH DOM
var somElement = document.querySelector('.foo');
console.log(somElement.value);
var UserClass = /** @class */ (function () {
    function UserClass(fname, lname) {
        this.fname = fname;
        this.lname = lname;
        this.unchangableFname = fname;
    }
    UserClass.prototype.getFullName = function () {
        return this.fname + ' ' + this.lname;
    };
    UserClass.maxAge = 50;
    return UserClass;
}());
var AdminClass = /** @class */ (function (_super) {
    __extends(AdminClass, _super);
    function AdminClass() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AdminClass;
}(UserClass));
var user = new UserClass('Tanisha', 'Bisht');
console.log(UserClass.maxAge); // static
// GENERICS WITH FUNCTION
var addId = function (obj) {
    var id = Math.random().toString(16);
    return __assign(__assign({}, obj), { id: id });
};
var objData = { name: 'Tanisha' };
var result = addId(objData);
console.log('result', result);
var genericData = {
    name: 'Tanisha',
    data: 347
};
// ENUMS: can be used as data type as well
var Status;
(function (Status) {
    Status[Status["NotStarted"] = 0] = "NotStarted";
    Status[Status["InProgress"] = 1] = "InProgress";
    Status[Status["Done"] = 2] = "Done";
})(Status || (Status = {}));
var notStartedStatus = Status.NotStarted;
console.log(Status.Done); // 2
